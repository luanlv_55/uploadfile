package controllers;

import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Http.MultipartFormData.FilePart;

import play.data.Form;
import static play.data.Form.form;

import models.Files;
import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render(
                form(UploadFilesForm.class),
                Files.find.all()
            ));
    }

    public static Result getFile(long id, String filename) {
        Files file = Files.find.byId(id);

        if (file != null) {
            return ok(file.data);

        } else {
            flash("error", "File not found.");
            return redirect(routes.Application.index());
        }
    }

    public static Result uploadFiles() {
        Form<UploadFilesForm> form = form(UploadFilesForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(index.render(
                    form,
                    Files.find.all()
                ));

        } else {
            new Files(
                    form.get().file.getFilename(),
                    form.get().file.getFile()
                );

            flash("success", "File uploaded.");
            return redirect(routes.Application.index());
        }
    }

    public static class UploadFilesForm {
        public FilePart file;
        
        public String validate() {
            MultipartFormData data = request().body().asMultipartFormData();
            file = data.getFile("dulieu");
            
            if (file == null) {
                return "File is missing.";
            }
            
            return null;
        }
    }
}
