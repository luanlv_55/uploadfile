package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import play.db.ebean.Model;
import play.data.validation.Constraints;

import java.io.File;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

@Entity
@Table(name="file")
public class Files extends Model {
    private static final long serialVersionUID = 1L;
    
    @Id
    public long id;
    
    @Constraints.Required
    public String name;

    @Lob
    public byte[] data;
    
    public static Finder<Long, Files> find = 
            new Finder<Long, Files>(Long.class, Files.class);
    
    public Files(String name, File file) {
        this.name = name;
        this.data = new byte[(int)file.length()];

        InputStream inStream = null;
        try {
            inStream = new BufferedInputStream(new FileInputStream(file));
            inStream.read(this.data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        this.save();
    }
}
